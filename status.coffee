# Description:
#   Generates status page for Hubot.
#
# URLS:
#   /status
#
# Notes:
#   These commands are grabbed from comment blocks at the top of each file.

fs = require "fs"

module.exports = (robot) ->
  #robot.webui.addMenu "Status", "/status", "check-circle", 0

  robot.router.get "/status", (req, res) ->
    robot.webui.activateMenu "/status"

    env = Object.keys(process.env)
      .sort()
      .map (key) -> ({ key: key, value: process.env[key] })

    avatar = process.env.MATTERMOST_ICON_URL
    avatar = process.env.SLACK_ICON_URL unless avatar
    avatar = process.env.HUBOT_ICON_URL unless avatar
    avatar = "/hubot.png" unless avatar

    fs.readFile "#{__dirname}/../views/status.html", (err, content) ->
      res.setHeader 'content-type', 'text/html'
      res.end robot.webui.render content.toString(), { env: env, avatar: avatar }
