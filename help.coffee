# Description:
#   Generates help commands for Hubot.
#
# Commands:
#   hubot help - Displays all of the help commands that Hubot knows about.
#   hubot help <query> - Displays all help commands that match <query>.
#
# URLS:
#   /help
#
# Notes:
#   These commands are grabbed from comment blocks at the top of each file.

fs = require "fs"

module.exports = (robot) ->
  commands = (robot) ->
    robot.helpCommands()
      .map (command) -> command.replace /hubot/ig, robot.name
      .sort (a, b) ->
        a = a.replace /\[|\(|<|>|\)|\]/g, ''
        b = b.replace /\[|\(|<|>|\)|\]/g, ''

        return -1 if a < b
        return 1 if a > b
        0

  robot.respond /help(?:\s+(.*))?$/i, (msg) ->
    filter = msg.match[1]
    cmds = commands(robot)
      .map (command) -> command.replace /<([^>]+)>/ig, "`$1`"
      .map (command) -> command.replace new RegExp("^#{robot.name}", 'ig'), "*#{robot.name}*"

    if filter
      cmds = cmds.filter (cmd) ->
        cmd.match new RegExp(filter, 'i')
      if cmds.length == 0
        msg.send "No available commands match #{filter}"
        return

    emit = cmds.join "\n"

    msg.send emit

  # robot.webui.addMenu "Help", "/help", "question-circle", 99

  robot.router.get "/help", (req, res) ->
    robot.webui.activateMenu "/help"

    cmds = commands(robot)
      .map (command) -> command.replace /<([^>]+)>/ig, "<code>$1</code>"
      .map (command) -> command.replace new RegExp("^#{robot.name}", 'ig'), "<strong>#{robot.name}</strong>"

    fs.readFile "#{__dirname}/../views/help.html", (err, content) ->
      res.setHeader 'content-type', 'text/html'
      res.end robot.webui.render content.toString(), { commands: cmds }
